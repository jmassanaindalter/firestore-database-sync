import * as functions from "firebase-functions";
import * as admin from "firebase-admin";
import * as appfb from "firebase/app";
import * as express from "express";
// // Start writing Firebase Functions
// // https://firebase.google.com/docs/functions/typescript
//
// export const helloWorld = functions.https.onRequest((request, response) => {
//   functions.logger.info("Hello logs!", {structuredData: true});
//   response.send("Hello from Firebase!");
// });

admin.initializeApp({
    credential: admin.credential.cert("./cifo-test-app-firebase-adminsdk-bsvjk-a3032c5505.json"),
    databaseURL: "https://cifo-test-app-default-rtdb.europe-west1.firebasedatabase.app"
});

const firebaseConfig = {
    apiKey: "AIzaSyB6dE189KFw7TCvultSKZhxy6cAcaTidL4",
    authDomain: "cifo-test-app.firebaseapp.com",
    projectId: "cifo-test-app",
    storageBucket: "cifo-test-app.appspot.com",
    messagingSenderId: "345885544509",
    appId: "1:345885544509:web:c8bb6852bda540de0c5a54"
};

appfb.initializeApp(firebaseConfig);

const app = express();

app.get('/hello-world',(req,res) =>{

    return res.status(200)
                .json({message:"Hello world request suceed"});
});


const db = admin.firestore();

app.get('/create-users',async (req,res) =>{

    for(var i = 0; i < 20; i++){

        const docRef = db.collection('users').doc();

        var user = {
            name: "user"+i,
            email: "user"+i+"@test.com",
            online: false,
        }

        await docRef.create(user);

    }

    return res.status(200)
                .json({message:"Users created successfully"});
});


exports.onUsersStatusChanged = functions.database.ref('/status/{name}')
                            .onUpdate(
            async(change,context)=>{
                    
                const eventStatus = change.after.val();

                var docs = db.collection('users')
                                    .where("name","==",context.params.name);

                var docsGet = await docs.get();

                var nameDocument = docsGet.docs[0];

                var userRef = db.collection('users').doc(nameDocument.id);

                var online = false;
                if(eventStatus.state == "online"){
                        online = true;
                }

                userRef.update({
                    "online":online
                });



                

                
            })





exports.app = functions.https.onRequest(app);